var fs = require('fs');
var helper = {
    ToBinary: function(image){
        var base64Data = image.replace(/^data:image\/(png|gif|jpeg);base64,/,'');
        base64Data += base64Data.replace('+', ' ');
        var buff = new Buffer(base64Data, 'base64').toString('binary');
        var targetPath = './public/uploads/incidents/'+ helper.getTimeInSeconds() +'incident.jpg';
        fs.writeFile(targetPath, buff, 'binary', function (err) {
            if (!err){
                return true;
            }else{
                console.log(err);
                return false;
            }
        });
    },
    getTimeInSeconds: function () {
        var date = new Date();
        return date.getSeconds()+ ''+date.getMinutes()+''+ date.getHours();
    }
}