var express = require('express');
var router = express.Router();
var database = require('../services/database');

router.get('/', function(req, res){

});

router.post('/create', function(req, res){
    var u =  req.body.user;
    database.user.createUser(u).then(function(rows){
        console.log(rows);
        res.json(rows);
    }).catch(function(err){
        console.log(err);
    });
});

router.put('/edit/:id', function(req, res){

});

router.get('/show/:id', function(req, res){

});

router.delete('/remove/:id', function(req, res){

});

router.post('/sign_in', function(req, res){
    if(!req.session.user){
        var user = req.body.user;
        database.user.selectUser(user.phone).then(function(rows){
            if(rows.length){
                if(rows[0].admin == 0) req.session.admin = true;
                if(rows[0].password == user.password){
                    req.session.user = user;
                    res.json({user: rows[0]});
                }else{
                    res.json({user: null, sign_in: false, message: 'contraseña incorrecta'});
                }
            }else{
                console.log(rows);
                res.json({errors: 'Usuario no encontrado'});
            }

        }).catch(function(err){
            res.json({errors: 'ups algo salio mal'});
            console.log(err);
        });
    }else{
        res.json({user: req.session.user, sign_in: true, message: 'ya haz iniciado sesion ;)'});
    }

});

router.delete('/sign_out', function(req, res){

});

module.exports = router;