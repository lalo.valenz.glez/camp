var express = require('express');
var router = express.Router();
var database = require('../services/database');
var moment = require('moment');


router.get('/', function(req, res){
    console.log('GET incident/');
    database.incident.getAllIncidents().then(function(rows){
        if(rows.length) {
            res.json({incidents: rows});
        }else{
            res.json({message: 'No hay Incidentes registrados'});
        }
    }).catch(function(err){
        console.log(err);
        res.json({errors: 'ha ocurrido un problema ):'});
    });
});

router.post('/create', function(req, res){
    console.log('POST /incident/create');
    if(req.session.user){
        var incident =  req.body.incident;
        database.incident.createIncident(incident).then(function(rows){
            res.status(201).json({incident:rows});
        }).catch(function(err){
            conosle.log(err);
            res.status(204).json({errors: 'ha ocurrod un error'});
        });
        console.log(incident);
    }else{
        res.json({incident: null, sign_in:false, message: 'no haz iniciado sesion'});
    }

});

router.put('/edit/:id', function(req, res){
    if(req.session.user){
        var id = req.params.id;
        database.incident.updateIncident(id).then(function(rows){
           if(rows.length){
                res.json({incident: rows});
           } else{
                res.json({message: 'no se encontro o no existe el incidente'});
           }
        }).catch(function(err){
            res.json({errors:'ha ocurrido algo'});
        });
    }else{
        res.json({errors:' no has iniciado sesion'});
    }
});

router.get('/show/:id', function(req, res){

});

router.delete('/remove/:id', function(req, res){

});

module.exports = router;