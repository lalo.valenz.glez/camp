var express = require('express');
var router = express.Router();
var database = require('../services/database');

router.get('/', function(req, res){
    console.log('GET payment/');
    database.payment.getAll().then(function(rows){
        if(rows.length) {
            res.json({payment: rows});
        }else{
            res.json({message: 'No hay Incidentes registrados'});
        }
    }).catch(function(err){
        console.log(err);
        res.json({errors: 'ha ocurrido un problema ):'});
    });
});

router.post('/create', function(req, res){
    console.log('POST /payment/create');
    if(req.session.user){
        var payment =  req.body.payment;
        database.payment.create(payment).then(function(rows){
            res.status(201).json({incident:rows});
        }).catch(function(err){
            conosle.log(err);
            res.status(204).json({errors: 'ha ocurrido un error'});
        })
        console.log(incident);
    }else{
        res.json({incident: null, sign_in:false, message: 'no haz iniciado sesion'});
    }
});

router.put('/edit/:id', function(req, res){

});

router.get('/show/:id', function(req, res){

});

router.delete('/remove/:id', function(req, res){

});

module.exports = router;