var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var mysql = require('promise-mysql');
var cors = require('cors');
var consolidate = require('consolidate');
var path = require('path');
var moment = require('moment');
var app = express();
var userController = require('./controllers/userController');
var incidentController = require('./controllers/incidentController');
var paymentController = require('./controllers/paymentsController');

app.use('/public', express.static('./public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser({limit: '50mb', extended: true, parameterLimit:50000}));
app.use(cookieParser());
app.use(cors());
app.use(session({ secret: 'tupapa', cookie: { maxAge: 60000 }, resave: true, saveUninitialized: true }));

app.use('/users', userController);
app.use('/incident', incidentController);
app.use('/payment', paymentController);

app.listen(3000, function(){
    console.log('app listening on localhost:3000');
})