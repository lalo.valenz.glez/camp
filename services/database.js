var mysql = require('promise-mysql');
var connection;
var mysqlOptions = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'camp'
};
mysql.createConnection(mysqlOptions).then(function(conn){
    connection = conn;
}).catch(function(err){
    console.log(err);
});


var database = {
    user: {
        createUser: function(user){
            console.log(user);
            return connection.query('insert into users (name, last_name, phone, password, address)' +
                'values(?, ?, ?, ?, ?)', [user.name, user.last_name, user.phone, user.password, user.address]);
        },
        selectUser: function (phone) {
            console.log(phone);
            var query = 'select * from users where phone = ?';
            return connection.query(query, [phone]);
        },
    },
    incident: {
        createIncident: function(incident){
            console.log(incident);
            var query = ' insert into incidents(user_id, description, photo, address, date_incident, checked) values(?, ?, ?, ?, now(), 0)';
            return connection.query(query, [1/*incident.user_id*/, incident.description, incident.image, incident.address])
        },
        getAllIncidents: function(){
            var query = 'select * from incidents';
            return connection.query(query,[]);
        },
        updateIncident: function(id){
            var query = 'update incident set checked = 1 where incident_id = ?';
            return connection.query(query, [id]);
        }
    },
    payment:{
        getAll: function(){
            var query = 'select * from payments';
            return connection.query(query, []);
        },
        create: function(payment){
            var query = 'insert into payments (user_id, payment_date, status, photo) values(?, now(), ?, ?)';
            return connection(query, [payment.user_id, payment.status, payment.photo]);
        }
    }
};

module.exports = database;